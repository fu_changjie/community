
# Application to create a new SIG
English | [简体中文](./sig-bounds_checking_function.md)


Note: The Charter of this SIG follows the convention described in the openEuler charter [README] (/en/governance/README.md), and follows [SIG-governance] (/en/technical-committee/governance/SIG-governance.md).

## SIG Mission and Scope

Describe the Mission and objectives of the new SIG, including but not limited to:

- provides the basic boundary checking function source code

- handles the release, update, and maintenance of boundary checking functions


 
### Repositories and description managed by this SIG

- project name: bounds_checking_function

- delivery form: source code

- name: bounds_checking_function


