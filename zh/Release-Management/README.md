# 发行经理

Release Manager的主要工作职责是负责规划社区版本、指定社区版本发布计划，协调各SIG的Maintainer、QA等各个团队，完成openEuler社区版本的发布工作。主要职责如下：

- 规划和计划openEuler版本的发行时间表
- 社区版本开发期间，跟踪需求和对应软件包的开发状态
- 帮助QA推动社区版本的关键问题解决
- 组织社区版本的发布评审会议
- 负责openEuler项目的交付过程协调



## 组织会议

公开的视频会议时间：北京时间每双周二上午，9:15~10:45，请订阅dev@openeuler.org，以获取相关会议通知



## 成员

- 江裕民[[@yuming_jiang](https://gitee.com/yuming_jiang)]
- 姜振华[[@Ronnie_Jiang](https://gitee.com/Ronnie_Jiang)]
- 管延杰 [[@guanyanjie](https://gitee.com/guanyanjie)]
- 刘博 [[@boliurise](https://gitee.com/boliurise)]
- 朱延鹏 [[@zyp-rock](https://gitee.com/zyp-rock)]



## 联系方式

邮箱列表：dev@openeuler.org



## 项目清单

### Release Manager

（*发布经理团队的相关信息均在以上Reository内，可以进入该Repository了解详情*）

Repository地址：https://gitee.com/openeuler/release-management

